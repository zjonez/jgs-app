import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';

// pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

//providers
import { BuyersProvider } from '../providers/buyers/buyers';

//pipes
import { PipesModule } from '../pipes/pipes.module'
import { AgentsProvider } from '../providers/agents/agents';

import { LotsProvider } from '../providers/lots/lots';
import { PaymentsProvider } from '../providers/payments/payments';
import { CommissionsProvider } from '../providers/commissions/commissions';
import { ConfigServiceProvider } from '../providers/config-service/config-service';
import { ProvinceServiceProvider } from '../providers/province-service/province-service';
import { CityServiceProvider } from '../providers/city-service/city-service';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage
  ],
  imports: [
    PipesModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicPageModule.forChild(HomePage),
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage
  ],
  providers: [
    HttpClientModule,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    
    //providers
    AuthServiceProvider,
    BuyersProvider,
    AgentsProvider,
    LotsProvider,
    PaymentsProvider,
    CommissionsProvider,
    ConfigServiceProvider,
    ProvinceServiceProvider,
    CityServiceProvider,
    LocalStorageProvider
  ]
})
export class AppModule {}
