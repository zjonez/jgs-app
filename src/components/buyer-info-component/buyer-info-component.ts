import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import * as _ from 'lodash';

/**
 * Generated class for the BuyerInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'buyer-info-component',
  templateUrl: 'buyer-info-component.html'
})
export class BuyerInfoComponent {

  public rForm: FormGroup;
  
  @Input('buyer') buyer: Object;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.rForm = this.fb.group({
      'buyerID': [],
      'buyer': []
    })
  }
}
