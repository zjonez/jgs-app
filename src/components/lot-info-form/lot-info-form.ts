import { Component, OnInit, Input} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import * as _ from 'lodash';

/**
 * Generated class for the LotInfoFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'lot-info-form',
  templateUrl: 'lot-info-form.html'
})
export class LotInfoFormComponent implements OnInit {

  public rForm: FormGroup;

  @Input('lot') lot: Object;
  @Input('buyer') buyer: Object;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.rForm = this.fb.group({
      'buyerID': [],
      'buyer': [],
      'lotType': [],
      'phase': [],
      'block': [],
      'section': [],
      'lotNo': [],
      'lotPrice': [],
      'pcf': [],
      'totalPrice': [],
      'agentID': [],
      'firstName': [],
      'middleName': [],
      'lastName': []
    });

    if (!_.isNil(this.lot)) {
      this.setInitialValues(this.lot);
    }
  }

  private setInitialValues(params) {
    let initialValues: Object = {};
    _.forOwn(this.rForm.controls, function (value, field) {
        initialValues[field] = !_.isNil(params[field]) ? params[field] : null;
    });

    this.rForm.setValue(initialValues);
  }
}
