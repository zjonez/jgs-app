import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaymentsProvider } from '../../providers/payments/payments';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

import * as _ from 'lodash';

/**
 * Generated class for the LotPaymentFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

const FIELD_VALID = "VALID";
const FORM_VALID = "VALID";

@Component({
  selector: 'lot-payment-form-component',
  templateUrl: 'lot-payment-form-component.html',
  providers: [
    PaymentsProvider
  ]
})
export class LotPaymentFormComponent implements OnInit {

  public rForm: FormGroup;
  public formSubmitted: boolean = false;
  public cashPayment: boolean = false;
  public checkPayment: boolean = false;

  public data: any;

  // @TODO: this should be in DB
  public transactionTypes: Array<Object>;

  public paymentModes = [
    { key: 'cash', value: 'Cash' },
    { key: 'check', value: 'Check' }
  ];

  @Input('buyer') buyer;
  @Input('lot') lot;

  constructor(private fb: FormBuilder, private paymentService: PaymentsProvider,
  public local: LocalStorageProvider
  ) {
    let data: any = this.local.getItem('data');
    this.data = JSON.parse(data);
  }

  ngOnInit() {
    this.buildForm();
    this.getTransactionTypes();
    this.onChanges();
  }

  onChanges(): void {
    this.rForm.get('paymentMode').valueChanges.subscribe(val => {
      this.isPaymentMode();
    });
  }

  public isFieldValid(field: any): boolean {
    return this.rForm.controls[field].status === FIELD_VALID;
  }

  public isFieldEmpty(field: any): boolean {
    return _.isEmpty(this.rForm.controls[field].value);
  }

  public isFormSubmitted(): boolean {
    return this.formSubmitted;
  }

  public submitForm(values: Object) {
    if (this.isFormValid(this.rForm.status)) {
      let lotID = this.lot.lotID;
      let employeeID = this.data.employeeID;
      this.addPayment(values, lotID, employeeID);
    } else {
      console.log('Form not valid');
    }
    this.formSubmitted = true;
  }

  private addPayment(params, lotID: Number, employeeID: Number) {
    params['lotID'] = lotID;
    params['employeeID'] = employeeID;

    this.paymentService
    .addPayment(params)
    .then((response) => {
      console.log('response');
      console.log(response);
    })
  }

  private isFormValid(formStatus: string): boolean {
    return formStatus === FORM_VALID;
  }

  public isPaymentMode(): void {
    let paymentMode = this.rForm.controls['paymentMode'].value;

    if (paymentMode == 'cash') {
      this.cashPayment = true;
    } else {
      this.cashPayment = false;
    }
    if (paymentMode == 'check') {
      this.checkPayment = true;
    } else {
      this.checkPayment = false;
    }
  }

  private getTransactionTypes(): void {
    this.paymentService.getTransactionTypes().then(
      (response: Array<Object>) => {
        this.transactionTypes = response;
      }
    )
  }

  private buildForm(): void {
    this.rForm = this.fb.group({
      'transactionType': [null, Validators.required],
      'paymentMode': [null, Validators.required],
      'bank': [],
      'checkNo': [],
      'orNo': [],
      'prNo': [],
      'amount': [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }
}
