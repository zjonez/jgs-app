import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, Loading, AlertController, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProvinceServiceProvider } from '../../providers/province-service/province-service';
import { CityServiceProvider } from '../../providers/city-service/city-service';
import { BuyersProvider } from '../../providers/buyers/buyers';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

import * as _ from 'lodash';

/**
 * Generated class for the BuyerFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

const FIELD_VALID = "VALID";
const FORM_VALID = "VALID";
const HOME_PROVINCE = 'home-province';
const MAIL_PROVINCE = 'mail-province';

@Component({
  selector: 'buyer-form',
  templateUrl: 'buyer-form.html',
  providers: [
    ProvinceServiceProvider,
    CityServiceProvider
  ]
})

export class BuyerFormComponent implements OnInit {

  public rForm: FormGroup;

  public formSubmitted: boolean = false;
  public married: boolean = false;

  public provinces: Array<Object>;
  public homeCities: Array<Object>;
  public mailCities: Array<Object>;

  public homeProvince: Object;
  public mailProvince: Object;

  public data: any;
  public loading: Loading;

  public civilStatuses = [
    { key: "Single", description: "Single" },
    { key: "Married", description: "Married" },
    { key: "Divorced", description: "Divorced" },
    { key: "Separated", description: "Separated" },
    { key: "Widowed", description: "Widowed" }
  ];

  @Input('showBuyerID') showBuyerID: boolean;
  @Input('buyer') buyer: Object;

  constructor(
    public fb: FormBuilder, public alertCtrl: AlertController,
    public nav: NavController, public loadingCtrl: LoadingController,
    public local: LocalStorageProvider, public cityService: CityServiceProvider,
    public buyerService: BuyersProvider, public provinceService: ProvinceServiceProvider
  ) {
    let data: any = this.local.getItem('data');
    this.data = JSON.parse(data);
  }

  ngOnInit() {
    this.buildForm();
    this.getProvinces();
    this.onChanges();
  }

  //@TODO: make province and city searchable (dropdown with search) (nice to have)
  onChanges(): void {
    this.rForm.get('civilStatus').valueChanges.subscribe(val => {
      if (val === 'Married') {
        this.married = true;
      }
    });

    this.rForm.get('homeProvince').valueChanges.subscribe(val => {
      this.rForm.controls['homeCity'].disable();
      let provinceObj: Object = _.find(this.provinces, (o) => { return o.name == val; });
      this.getCities(provinceObj, HOME_PROVINCE);
    });

    this.rForm.get('mailProvince').valueChanges.subscribe(val => {
      this.rForm.controls['mailCity'].disable();
      let provinceObj: Object = _.find(this.provinces, (o) => { return o.name == val; });
      this.getCities(provinceObj, MAIL_PROVINCE);
    });

    this.rForm.get('sameAsAbove').valueChanges.subscribe(val => {
      if (val === true) {
        this.populateMailAddressFromHome();
      } else {
        this.clearMailingAddress();
      }
    })
  }

  public isFieldValid(field: any): boolean {
    return this.rForm.controls[field].status === FIELD_VALID;
  }

  public isFieldEmpty(field: any): boolean {
    return _.isEmpty(this.rForm.controls[field].value);
  }

  public isFormSubmitted(): boolean {
    return this.formSubmitted;
  }

  public submitForm(values: Object) {
    if (this.isFormValid(this.rForm.status)) {
      let employeeID = this.data.employeeID;
      this.addBuyer(values, employeeID);
    } else {
      console.log('form not valid');
    }
    this.formSubmitted = true;
  }

  private addBuyer(params, employeeID: Number) {
    this.showLoading();
    params['employeeID'] = employeeID;

    this.buyerService
      .addBuyer(params)
      .then((response: any) => {
        this.loading.dismiss();
        if (response.success) {
          this.showAlert(response.message, true);
        } else {
          this.showAlert(response.message, false);
        }
      });
  }

  private showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Adding buyer...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private showAlert(text, status) {
    let title = !status ? 'Error' : 'Success';
    text = !(_.isNil(text)) ? text : 'Unknown error occured';
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (status === true) {
              this.nav.push('BuyersPage');
            }
          }
        }
      ]
    });
    alert.present();
  }

  private isFormValid(formStatus: string): boolean {
    return formStatus === FORM_VALID;
  }

  private populateMailAddressFromHome(): void {
    let homeAddressLine1 = this.rForm.controls['homeAddressLine1'].value;
    let homeAddressLine2 = this.rForm.controls['homeAddressLine2'].value;
    let homeBarangay = this.rForm.controls['homeBarangay'].value;
    let homeProvince = this.rForm.controls['homeProvince'].value;
    let homeCity = this.rForm.controls['homeCity'].value;
    let homePostalCode = this.rForm.controls['homePostalCode'].value;

    this.rForm.controls['mailAddressLine1'].setValue(homeAddressLine1);
    this.rForm.controls['mailAddressLine2'].setValue(homeAddressLine2);
    this.rForm.controls['mailBarangay'].setValue(homeBarangay);
    this.rForm.controls['mailPostalCode'].setValue(homePostalCode);

    if (!_.isNull(homeProvince)) {
      this.rForm.controls['mailProvince'].setValue(homeProvince);
    }

    if (!_.isNull(homeCity)) {
      this.rForm.controls['mailCity'].setValue(homeCity);
    }

  }

  private clearMailingAddress(): void {
    this.rForm.controls['mailAddressLine1'].setValue(null);
    this.rForm.controls['mailAddressLine2'].setValue(null);
    this.rForm.controls['mailBarangay'].setValue(null);
    this.rForm.controls['mailProvince'].setValue(null);
    this.rForm.controls['mailCity'].setValue(null);
    this.rForm.controls['mailPostalCode'].setValue(null);
  }

  private getProvinces(): void {
    this.provinceService.getProvinces().then(
      (provinces: Array<Object>) => {
        this.provinces = provinces;
      }
    )
  }

  private getCities(province, type): void {
    this.cityService.getCities(province).then(
      (cities: Array<Object>) => {
        if (type === HOME_PROVINCE) {
          this.homeCities = cities;
          this.rForm.controls['homeCity'].enable();
        } else {
          this.mailCities = cities;
          this.rForm.controls['mailCity'].enable();
        }
      }
    )
  }

  private buildForm(): void {
    this.rForm = this.fb.group({
      'buyerID': [],
      'firstName': [null, Validators.required],
      'middleName': [null, Validators.compose([Validators.required, Validators.minLength(1)])],
      'lastName': [null, Validators.required],
      'mobileNo': [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
      'landlineNo': [null, Validators.compose([Validators.minLength(7), Validators.maxLength(7)])],
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'birthDate': [null, Validators.required],
      'gender': [null, Validators.required],
      'civilStatus': [null, Validators.required],
      'spouse': [],
      'homeAddressLine1': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      'homeAddressLine2': [],
      'homeBarangay': [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      'homeProvince': [null, Validators.required],
      'homeCity': [null, Validators.required],
      'homePostalCode': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
      'sameAsAbove': [],
      'mailAddressLine1': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      'mailAddressLine2': [],
      'mailBarangay': [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      'mailProvince': [null, Validators.required],
      'mailCity': [null, Validators.required],
      'mailPostalCode': [null, Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.rForm.controls['homeCity'].disable();
    this.rForm.controls['mailCity'].disable();

    if (!_.isNil(this.buyer)) {
      this.setInitialValues(this.buyer);
    }
  }

  private setInitialValues(buyer) {
    let initialValues: Object = {};
    _.forOwn(this.rForm.controls, function (value, field) {
      initialValues[field] = !_.isNil(buyer[field]) ? buyer[field] : null;
    });

    this.rForm.setValue(initialValues);
  }
}
