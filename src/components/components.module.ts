import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AgentFormComponent } from "../components/agent-form/agent-form";
import { BuyerFormComponent } from "../components/buyer-form/buyer-form";
import { LotFormComponent } from "../components/lot-form-component/lot-form-component";
import { UserHeaderComponent } from "./user-header-component/user-header-component";

/**
 * Only import components here if it will be used on multiple pages.
 * Else, import it on its own module.
 */
@NgModule({
  declarations: [
    AgentFormComponent,
    BuyerFormComponent,
    LotFormComponent,
    UserHeaderComponent
  ],
  exports: [
    AgentFormComponent,
    BuyerFormComponent,
    LotFormComponent,
    UserHeaderComponent
  ],
  imports: [IonicModule, FormsModule, ReactiveFormsModule]
})
export class ComponentsModule {}
