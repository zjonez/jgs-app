import { Component } from "@angular/core";
import { NavController, ActionSheetController, NavParams } from "ionic-angular";
import { LocalStorageProvider } from "../../providers/local-storage/local-storage";
import * as _ from "lodash";

/**
 * Generated class for the UserHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "user-header-component",
  templateUrl: "user-header-component.html"
})
export class UserHeaderComponent {
  public user: Object;

  constructor(
    public nav: NavController,
    public local: LocalStorageProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.getCurrentUser();
  }

  private getCurrentUser(): void {
    let data: any = this.local.getItem("data");

    if (!_.isEmpty(data)) {
      this.user = JSON.parse(data);
    } else {
      this.nav.push("LoginPage");
    }
  }

  public openOptions(): void {
    let fullName = this.getFullName(this.user);
    let actionSheet = this.actionSheetCtrl.create({
      title: fullName,
      buttons: [
        {
          text: "Home",
          handler: () => {
            window.location.href = "/";
          }
        },
        {
          text: "Logout",
          handler: () => {
            this.logout();
          }
        },
        {
          text: "Close",
          role: "cancel",
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

  public getFullName(user): string {
    return user.firstName + " " + user.lastName;
  }

  public logout(): void {
    this.local.clear();
    location.reload();
  }
}
