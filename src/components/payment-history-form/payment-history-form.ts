import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { PaymentsProvider } from '../../providers/payments/payments';

import * as _ from 'lodash';

/**
 * Generated class for the PaymentHistoryFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'payment-history-form',
  templateUrl: 'payment-history-form.html',
  providers: [
    PaymentsProvider
  ]
})
export class PaymentHistoryFormComponent {

  public payments;

  public isApp: boolean;

  constructor(public paymentService: PaymentsProvider, public platform: Platform
  ) {
    this.platform = platform;
    this.isApp = this.platform.is('core') ? false : true;

    this.getPaymentsFromService();
  }

  public getPaymentsFromService() {
    this.paymentService.getPayments().then(
      (payments) => {
        this.payments = payments;
      }
    )
  }

}
