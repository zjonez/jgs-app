import { Component, OnInit, Input } from '@angular/core';
import { LoadingController, Loading, AlertController, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProvinceServiceProvider } from '../../providers/province-service/province-service';
import { CityServiceProvider } from '../../providers/city-service/city-service';
import { AgentsProvider } from '../../providers/agents/agents';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

import * as _ from 'lodash';

/**
 * Generated class for the AgentFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

const FIELD_VALID = "VALID";
const FORM_VALID = "VALID";
const HOME_PROVINCE = 'home-province';

@Component({
  selector: 'agent-form',
  templateUrl: 'agent-form.html',
  providers: [
    ProvinceServiceProvider,
    CityServiceProvider
  ]
})
export class AgentFormComponent implements OnInit {

  public rForm: FormGroup;
  public formSubmitted: boolean = false;
  public provinces: Array<Object>;
  public homeCities: Array<Object>;

  public data: any;
  public loading: Loading;

  public genders = [
    { key: "M", description: "Male"},
    { key: "F", description: "Female" }
  ];

  public civilStatuses = [
    'Single',
    'Married',
    'Divorced',
    'Separated',
    'Widowed'
  ];
  
  public agentTypes = [
    { key: "indie", description: "Independent" },
    { key: "sc", description: "SC" },
    { key: "bm", description: "BM" },
    { key: "asd", description: "ASD" }
  ];

  public homeProvince: Object;

  @Input('showAgentID') showAgentID: boolean;
  @Input('agent') agent: Object;

  constructor(
    private fb: FormBuilder, public alertCtrl: AlertController,
    public nav: NavController, public loadingCtrl: LoadingController,
    public provinceService: ProvinceServiceProvider,
    public cityService: CityServiceProvider, private agentService: AgentsProvider,
    public local: LocalStorageProvider
  ) {
    let data: any = this.local.getItem('data');
    this.data = JSON.parse(data);
  }

  ngOnInit() {
    this.buildForm();
    this.getProvinces();
    this.onChanges();
  }

  onChanges(): void {
    this.rForm.get('homeProvince').valueChanges.subscribe(val => {
      this.rForm.controls['homeCity'].disable();
      let provinceObj: Object = _.find(this.provinces, (o) => { return o.name == val; });
      this.getCities(provinceObj, HOME_PROVINCE);
    });
  }

  public isFieldValid(field: any): boolean {
    return this.rForm.controls[field].status === FIELD_VALID;
  }

  public isFieldEmpty(field: any): boolean {
    return _.isEmpty(this.rForm.controls[field].value);
  }

  public isFormSubmitted(): boolean {
    return this.formSubmitted;
  }

  public submitForm(values: Object) {
    if (this.isFormValid(this.rForm.status)) {
      let employeeID = this.data.employeeID;
      this.addAgent(values, employeeID);
    } else {
      console.log('Form not valid');
    }
    this.formSubmitted = true;
  }

  private getProvinces(): void {
    this.provinceService.getProvinces().then(
      (provinces: Array<Object>) => {
        this.provinces = provinces;
      }
    )
  }

  private getCities(province, type): void {
    this.cityService.getCities(province).then(
      (cities: Array<Object>) => {
        if (type === HOME_PROVINCE) {
          this.homeCities = cities;
          this.rForm.controls['homeCity'].enable();
        }
      }
    )
  }

  private addAgent(params, employeeID: Number) {
    this.showLoading();
    params['employeeID'] = employeeID;

    this.agentService
    .addAgent(params)
    .then((response: any) => {
      this.loading.dismiss();
      if (response.success) {
        this.showAlert(response.message, response.status);
      } else {
        this.showAlert(response.message, response.status);
      }
    });
  }

  private showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Adding agent...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private showAlert(text, status) {
    let title = !status ? 'Error' : 'Success';
    text = !(_.isNil(text)) ? text : 'Unknown error occured';
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (status) {
              this.nav.push('AgentsPage');
            }
          }
        }
      ]
    });
    alert.present();
  }

  private isFormValid(formStatus: string): boolean {
    return formStatus === FORM_VALID;
  }

  private buildForm() {
    this.rForm = this.fb.group({
      'agentID': [],
      'firstName': [null, Validators.required],
      'middleName': [null, Validators.required],
      'lastName': [null, Validators.required],
      'nickName': [null, Validators.required],
      'mobileNo': [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
      'landlineNo': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(7)])],
      'tin': [null, Validators.required],
      'email': [null, Validators.required],
      'birthDate': [null, Validators.required],
      'gender': [null, Validators.required],
      'civilStatus': [null, Validators.required],
      'spouse': [],
      'agentType': [null, Validators.required],
      'homeAddressLine1': [null, Validators.compose([Validators.required, Validators.minLength(10)])],
      'homeAddressLine2': [],
      'homeBarangay': [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      'homeProvince': [null, Validators.required],
      'homeCity': [null, Validators.required],
      'homePostalCode': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])]
    });

    this.rForm.controls['homeCity'].disable();
  
    if(!_.isNil(this.agent)) {
      this.setInitialValues(this.agent);
    }
  }

  private setInitialValues(agent) {
    let initialValues: Object = {};
    _.forOwn(this.rForm.controls, function (value, field) {
      initialValues[field] = !_.isNil(agent[field]) ? agent[field] : null;
    });

    this.rForm.setValue(initialValues);
  }
}
