import { Component, OnInit, Input, OnChanges } from "@angular/core";
import {
  LoadingController,
  Loading,
  AlertController,
  NavController
} from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LotsProvider } from "../../providers/lots/lots";
import { AgentsProvider } from "../../providers/agents/agents";
import { LocalStorageProvider } from "../../providers/local-storage/local-storage";
import { BuyersProvider } from "../../providers/buyers/buyers";

import * as _ from "lodash";

/**
 * Generated class for the LotFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

const FIELD_VALID = "VALID";
const FORM_VALID = "VALID";

@Component({
  selector: "lot-form-component",
  templateUrl: "lot-form-component.html",
  providers: [LotsProvider]
})

export class LotFormComponent implements OnInit {
  public rForm: FormGroup;
  public formSubmitted: boolean = false;

  public lotTypes: Array<Object>;
  public phases: Array<Number> = _.range(1, 8);
  public blocks: Array<Number> = _.range(1, 5);

  public data: any;
  public agents: Object;
  public loading: Loading;

  @Input("showAgent") showAgent: boolean;
  @Input("buyer") buyer;
  @Input("buyerLotPage") buyerLotPage: boolean;

  constructor(
    private fb: FormBuilder,
    public alertCtrl: AlertController,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    private lotService: LotsProvider,
    private agentService: AgentsProvider,
    public local: LocalStorageProvider,
    public buyerService: BuyersProvider
  ) {
    let data: any = this.local.getItem("data");
    this.data = JSON.parse(data);
  }

  ngOnInit() {
    this.buildForm();
    this.getAgentsFromService();
    this.getLotTypes();
    this.onChanges();
  }

  public getAgentsFromService() {
    this.showLoading();

    setTimeout(() => {
      this.agentService.getAgents().then((response: any) => {
        this.loading.dismiss();
        if (!response.success) {
          this.showError(response.message);
        } else if (!_.isNil(response.results)) {
          this.agents = response.results;
        }
      });
    }, 2000);
  }

  /**
   * @TODO: If all required fields are filled up, do search on the lot if available
   */
  onChanges(): void {
    this.rForm.valueChanges
    .subscribe((lotObj) => {
      if(this.isValidLot(lotObj) && this.buyerLotPage) {
        this.checkLotAvailability(lotObj);
      }
    });
  }

  public isFieldValid(field: any): boolean {
    if(!_.isNil(this.rForm.controls[field])) {
      return this.rForm.controls[field].status === FIELD_VALID;
    } else {
      return false;
    }
  }

  public isFieldEmpty(field: any): boolean {
    return _.isEmpty(this.rForm.controls[field].value);
  }

  public isFormSubmitted(): boolean {
    return this.formSubmitted;
  }

  public submitForm(values: Object) {
    if (this.isFormValid(this.rForm.status)) {
      if (this.showAgent) { // from add buyer lot page
        let buyerID = this.buyer.buyerID;
        let employeeID = this.data.employeeID;
        this.addLotBuyer(values, buyerID, employeeID);
      } else { // from add lot page
        let employeeID = this.data.employeeID;
        this.addLotInventory(values, employeeID);
      }
    } else {
      console.log("Form not valid");
    }

    this.formSubmitted = true;
  }

  private addLotInventory(params, employeeID: Number) {
    this.showLoading("Adding Lot");
    params["employeeID"] = employeeID;

    this.lotService.addLotInventory(params).then((response: any) => {
      this.loading.dismiss();

      if (response.success) {
        this.showAlert(response.message, true);
      } else {
        this.showAlert(response.message, false);
      }
    });
  }

  /**
   * @TODO: do search of lot before adding to DB, so that we can use the lotID found
   * to add to database. (Do search well all fields are filled up)
   * @param params 
   * @param buyerID 
   * @param employeeID 
   */
  private addLotBuyer(params, buyerID: Number, employeeID: Number) {
    this.showLoading("Adding Lot to buyer");
    params["employeeID"] = employeeID;

    const lotID = 1; // @TODO: should come from DB.
    this
      .buyerService
      .addLotBuyer(params, buyerID, lotID)
      .then((response: any) => {
        this.loading.dismiss();
        if (response.success) {
          this.showAlert(response.message, true);
        } else {
          this.showAlert(response.message, false);
        }
      });
  }

  private showLoading(text = "Please wait...") {
    this.loading = this.loadingCtrl.create({
      content: text,
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private showAlert(text, status) {
    let title = !status ? "Error" : "Success";
    text = !_.isNil(text) ? text : "Unknown error occured";
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: "OK",
          handler: data => {
            if (status) {
              this.nav.push("LotsPage");
            }
          }
        }
      ]
    });
    alert.present();
  }

  public showError(text) {
    text = !_.isNil(text) ? text : "Unknown error occured";

    let alert = this.alertCtrl.create({
      title: "login failed",
      subTitle: text,
      buttons: ["OK"]
    });

    alert.present();
  }

  private isFormValid(formStatus: string): boolean {
    return formStatus === FORM_VALID;
  }

  private getLotTypes(): void {
    this.lotService.getLotTypes().then((response: Array<Object>) => {
      this.lotTypes = response;
    });
  }

  private buildForm(): void {
    this.rForm = this.fb.group({
      lotType: [null, Validators.required],
      phase: [null, Validators.required],
      block: [null, Validators.required],
      section: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(2)])
      ],
      lotNo: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(2)])
      ],
      agentID: []
    });
  }

  private isValidLot(lotObj): boolean {
    for (const key of Object.keys(lotObj)) {
      if(_.isEmpty(lotObj[key])) {
        return false;
      }
    }

    return true;
  }

  private checkLotAvailability(lotObj) {
    this.showLoading("Checking Lot...");

    this
      .lotService
      .getLotDetails(lotObj)
      .then((response) => {
        console.log('response');
        console.log(response);
        this.loading.dismiss();
      });
  }
}
