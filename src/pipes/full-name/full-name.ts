import { Pipe, PipeTransform } from '@angular/core';

import * as _ from 'lodash';

/**
 * Generated class for the FullNamePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'fullName',
})
export class FullNamePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value, ...args) {
    let showFullName = args[0];

    if (!_.isNil(value)) {
      return this.displayName(value, showFullName);
    } else {
      console.error('Error, could not display name');
      return ' ';
    }
  }

  public displayName(value, showFullName) {
    let firstName = !_.isNil(value.firstName) ? value.firstName : "";
    let middleName = !_.isNil(value.middleName) ? value.middleName : "";
    let middleInitial = this.getMiddleInitial(middleName);
    let lastName = !_.isNil(value.lastName) ? value.lastName : "";
    let fullName = firstName + " " + middleName + " " + lastName;
    let withInitial = lastName + ", " + firstName + " " + middleInitial;

    if (!_.isNil(showFullName)) {
      return fullName;
    } else {
      return withInitial;
    }
  }

  public getMiddleInitial(middleName: string) {
    if(!_.isNil(middleName) && !_.isEmpty(middleName)) {
      return middleName[0].toUpperCase() + ".";
    }
    return '';
  }
}
