import { NgModule } from '@angular/core';
import { FullNamePipe } from './full-name/full-name';

// @TODO: remove middle initial pipe, not used
@NgModule({
	declarations: [
    	FullNamePipe
	],
	imports: [],
	exports: [
    	FullNamePipe
	]
})

export class PipesModule {
}
