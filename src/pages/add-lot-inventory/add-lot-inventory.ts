import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddLotInventoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-lot-inventory',
  templateUrl: 'add-lot-inventory.html',
})
export class AddLotInventoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
