import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddLotInventoryPage } from './add-lot-inventory';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddLotInventoryPage
  ],
  imports: [
    IonicPageModule.forChild(AddLotInventoryPage),
    ComponentsModule
  ],
})
export class AddLotInventoryPageModule {}
