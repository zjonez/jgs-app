import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { LocalStorageProvider } from "../../providers/local-storage/local-storage";

import * as _ from "lodash";

/**
 * Generated class for the AddBuyerLotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-add-buyer-lot",
  templateUrl: "add-buyer-lot.html"
})

export class AddBuyerLotPage {
  public buyer: Object;
  public data: Object = {};

  public lotTypes: Array<Object>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public local: LocalStorageProvider
  ) {
    this.buyer = navParams.data.buyer;

    if (!_.isNil(navParams.data) && !_.isEmpty(navParams.data)) {
      this.buyer = navParams.data.buyer;

      local.setItem("buyer", JSON.stringify(this.buyer));
    } else if (
      !_.isNil(local.getItem("buyer")) &&
      !_.isEmpty(local.getItem("buyer"))
    ) {
      this.buyer = JSON.parse(local.getItem("buyer"));
    } else {
      this.local.clear();
      window.location.href = "/";
    }
  }
}
