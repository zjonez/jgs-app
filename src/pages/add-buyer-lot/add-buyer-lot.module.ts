import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBuyerLotPage } from './add-buyer-lot';

import { BuyerInfoComponent } from '../../components/buyer-info-component/buyer-info-component';
import { ComponentsModule } from '../../components/components.module';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AddBuyerLotPage,
    BuyerInfoComponent
  ],
  imports: [
    IonicPageModule.forChild(AddBuyerLotPage),
    ComponentsModule,
    PipesModule
  ],
})
export class AddBuyerLotPageModule {}
