import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { LotsProvider } from '../../providers/lots/lots';
import * as _ from 'lodash';

/**
 * Generated class for the LotsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lots',
  templateUrl: 'lots.html'
})

export class LotsPage implements OnInit {

  public lots;
  public loading: any = Loading;

  constructor(
    public nav: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public lotService: LotsProvider
  ) {
  }

  ngOnInit() {
    this.getLotsFromService();
  }

  public getLotsFromService() {
    this.showLoading();

    setTimeout(() => {
      this.lotService
        .getLots()
        .then((response: any) => {
          this.loading.dismiss();
          if (!response.success) {
            this.showError(response.message);
          } else if (!_.isNil(response.results)) {
            this.lots = response.results;
          }
        })
    }, 2000);
  }

  public openLotInventory(lot: Object) {
    this.nav.push("LotInventoryPage", lot);
  }

  public openAddLotInventory(lot: Object) {
    this.nav.push("AddLotInventoryPage", lot);
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public showError(text) {
    text = !(_.isNil(text)) ? text : 'Unknown error occured';

    let alert = this.alertCtrl.create({
      title: 'login failed',
      subTitle: text,
      buttons: ['OK']
    });

    alert.present()
  }
}
