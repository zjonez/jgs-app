import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotsPage } from './lots';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    LotsPage,
  ],
  imports: [
    IonicPageModule.forChild(LotsPage),
    ComponentsModule
  ],
})
export class LotsPageModule {}
