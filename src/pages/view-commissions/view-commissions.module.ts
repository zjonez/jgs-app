import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewCommissionsPage } from './view-commissions';

@NgModule({
  declarations: [
    ViewCommissionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewCommissionsPage),
  ],
})
export class ViewCommissionsPageModule {}
