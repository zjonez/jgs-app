import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LotsProvider } from '../../providers/lots/lots';

/**
 * Generated class for the ViewCommissionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-commissions',
  templateUrl: 'view-commissions.html',
  providers: [
    LotsProvider
  ]
})
export class ViewCommissionsPage {

  public lots;

  public buyerID: String;
  public firstName: String;
  public middleName: String;
  public lastName: String;

  public option: String;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public lotService: LotsProvider
  ) {
    this.getLotsFromService();

    this.buyerID = navParams.get('buyerID');
    this.firstName = navParams.get('firstName');
    this.middleName = navParams.get('middleName');
    this.lastName = navParams.get('lastName');

    this.option = 'info';

  }

  // ionViewDidLoad() {
  //  console.log('ionViewDidLoad ViewCommissionsPage');
  // }
  public getLotsFromService() {
    this.lotService.getLots().then(
      (lots) => {
        this.lots = lots;
      }
    )
  }
}
