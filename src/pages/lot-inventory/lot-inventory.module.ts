import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotInventoryPage } from './lot-inventory';

@NgModule({
  declarations: [
    LotInventoryPage
  ],
  imports: [
    IonicPageModule.forChild(LotInventoryPage)
  ],
})
export class LotInventoryPageModule {}
