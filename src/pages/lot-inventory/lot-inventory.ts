import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LotInventoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lot-inventory',
  templateUrl: 'lot-inventory.html',
})
export class LotInventoryPage {

  public lotType: String;
  public phase: String;
  public block: String;
  public section: String;
  public lotNo: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.getNavParams(navParams);
  }

  public getNavParams(navParams: NavParams) {
    this.lotType = navParams.get('lotType');
    this.phase = navParams.get('phase');
    this.block = navParams.get('block');
    this.section = navParams.get('section');
    this.lotNo = navParams.get('lotNo');
  }

}
