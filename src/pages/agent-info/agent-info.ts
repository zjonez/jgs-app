import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { CommissionsProvider } from '../../providers/commissions/commissions';

/**
 * Generated class for the AgentInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agent-info',
  templateUrl: 'agent-info.html',
  providers: [
    CommissionsProvider
  ]
})
export class AgentInfoPage {

  public commissions;

  public agentID: String;
  public firstName: String;
  public middleName: String
  public lastName: String;
  public option: String;

  public isApp: boolean;

  public agent: Object;
  public data: Object = {};

  constructor(
    public nav: NavController, public navParams: NavParams, public commissionService: CommissionsProvider,
    public platform: Platform
  ) {
    this.getNavParams(navParams);
    this.option = 'info';

    this.platform = platform;
    this.isApp = this.platform.is('core') ? false : true;

    this.getCommissionsFromService();
    this.agent = navParams.data;
  }

  public getNavParams(navParams: NavParams) {
    this.agentID = navParams.get('agentID');
    this.firstName = navParams.get('firstName');
    this.middleName = navParams.get('middleName');
    this.lastName = navParams.get('lastName');
  }

  public getCommissionsFromService() {
    this.commissionService.getCommissions().then(
      (commissions) => {
        this.commissions = commissions;
      }
    );
  }

  public openViewCommissions(buyer: Object) {
    this.nav.push("ViewCommissionsPage", buyer);
  }
}
