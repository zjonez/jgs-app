import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgentInfoPage } from './agent-info';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    AgentInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(AgentInfoPage),
    ComponentsModule
  ],
})
export class AgentInfoPageModule {}
