import { Component } from '@angular/core';
import {
  IonicPage, NavController, AlertController,
  LoadingController, Loading,
} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { HomePage } from '../home/home';

import * as _ from 'lodash';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  loading: any = Loading;
  registerCredentials = { username: '', password: '' };

  private token: Object;

  constructor(
    private nav: NavController, private auth: AuthServiceProvider,
    private alertCtrl: AlertController, private loadingCtrl: LoadingController,
    private local: LocalStorageProvider
  ) {
    this.token = local.getItem('token');

    if (!_.isEmpty(this.token)) {
      this.nav.push(HomePage);
    }
  }

  public login() {
    this.showLoading();
    this.auth.login(this.registerCredentials)
      .then((res: any) => {
        this.loading.dismiss();
        if (!res.success) {
          this.showError(res.message);
        } else {
          this.local.setItem('token', res.token);
          this.local.setItem('data', JSON.stringify(res.data));
          this.nav.push(HomePage);
        }
      },
        (error) => {
          this.loading.dismiss();
          this.showError(error.message);
        }
      );
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    text = !(_.isNil(text)) ? text : 'Unknown error occured';

    let alert = this.alertCtrl.create({
      title: 'login failed',
      subTitle: text,
      buttons: ['OK']
    });

    alert.present()
  }
}
