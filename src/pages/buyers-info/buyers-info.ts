import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Loading,
  AlertController,
  ActionSheetController
} from "ionic-angular";
import { LotsProvider } from "../../providers/lots/lots";
import { LocalStorageProvider } from "../../providers/local-storage/local-storage";
import { BuyersProvider } from "../../providers/buyers/buyers";

import * as _ from "lodash";

/**
 * Generated class for the BuyersInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
  selector: "page-buyers-info",
  templateUrl: "buyers-info.html"
})

export class BuyersInfoPage implements OnInit {
  public lots;
  public loading: any = Loading;

  public buyerID: String;
  public firstName: String;
  public middleName: String;
  public lastName: String;
  public option: String;

  public buyer: any; // @TODO: this should be of type buyer
  public data: Object = {};

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public lotService: LotsProvider,
    public actionSheetCtrl: ActionSheetController,
    public local: LocalStorageProvider,
    public buyerService: BuyersProvider
  ) {
    if (!_.isNil(navParams.data) && !_.isEmpty(navParams.data)) {
      this.buyer = navParams.data;
      this.data["buyer"] = this.buyer;

      local.setItem("buyer", JSON.stringify(this.buyer));
    } else if (
      !_.isNil(local.getItem("buyer")) &&
      !_.isEmpty(local.getItem("buyer"))
    ) {
      this.buyer = JSON.parse(local.getItem("buyer"));
    } else {
      this.local.clear();
      window.location.href = "/";
    }

    this.option = "info";
  }

  ngOnInit() {
    this.getLotsFromService();
  }
  
  public getLotsFromService() {
    this.showLoading();

    setTimeout(() => {
      this.lotService.getLotBuyers().then((response: any) => {
        this.loading.dismiss();
        if (!response.success) {
          this.showError(response.message);
        } else if (!_.isNil(response.results)) {
          this.lots = response.results;
        }
      });
    }, 2000);
  }

  public openBuyerOptions(lot) {
    this.data["lot"] = lot;

    let actionSheet = this.actionSheetCtrl.create({
      title: "Please select action",
      buttons: [
        {
          text: "View",
          icon: "search",
          role: "view",
          handler: () => {
            this.nav.push("LotsInfoPage", this.data);
          }
        },
        {
          text: "Pay",
          icon: "cash",
          role: "pay",
          handler: () => {
            this.nav.push("AddPaymentPage", this.data);
          }
        },
        {
          text: "Contract",
          icon: "md-paper",
          role: "generate contract",
          handler: () => {
            console.log("Contract clicked");
            //Add button here to navigate to generate contract page
          }
        },
        {
          text: "Deed of Sale",
          icon: "ios-paper-outline",
          role: "generate deed of sale",
          handler: () => {
            console.log("Deed of Sale clicked");
            //Add button here to navigate to generate deed of sale page
          }
        },
        {
          text: "Certificate",
          icon: "ios-paper",
          role: "generate certificate",
          handler: () => {
            console.log("Certificate clicked");
            //Add button here to navigate to generate certificate page
          }
        }
      ]
    });
    actionSheet.present();
  }

  public openAddBuyerLot() {
    this.nav.push("AddBuyerLotPage", this.data);
    console.log("this.data");
    console.log(this.data);
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public showError(text) {
    text = !_.isNil(text) ? text : "Unknown error occured";

    let alert = this.alertCtrl.create({
      title: "login failed",
      subTitle: text,
      buttons: ["OK"]
    });

    alert.present();
  }
}
