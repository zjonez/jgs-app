import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyersInfoPage } from './buyers-info';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    BuyersInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyersInfoPage),
    ComponentsModule,
    PipesModule
  ],
})

export class BuyersInfoPageModule {}
