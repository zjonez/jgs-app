import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAgentPage } from './add-agent';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddAgentPage
  ],
  imports: [
    IonicPageModule.forChild(AddAgentPage),
    ComponentsModule
  ],
})
export class AddAgentPageModule {}
