import { Component } from '@angular/core';
import { NavController, ActionSheetController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  public menuItems: Array<Object>;

  public user: Object; //@TODO: add model for user
  public now: String; //@TODO: this should be type date

  constructor(
    public nav: NavController, public local : LocalStorageProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.now = moment().format('MMMM Do YYYY, h:mm:ss a');

    this.getMenuItems();
    this.getCurrentUser();
  }

  public getMenuItems() {
    this.menuItems = [
      { page: "myAccountPage", label: "My Account", disabled: true },
      { page: "LotsPage", label: "Lots", disabled: false },
      { page: "BuyersPage", label: "Buyers", disabled: false },
      { page: "AgentsPage", label: "Agents", disabled: false },
      { page: "filesPages", label: "Files", disabled: true },
      { page: "requestsPage", label: "Requests", disabled: true },
      { page: "notifPage", label: "Notifications", disabled: true } //@TODO: this can be moved elsewhere
    ]
  }

  public isItemDisabled(item): boolean {
    return item.disabled === true;
  }

  public openPage(page): void {
    this.nav.push(page);
  }

  private getCurrentUser(): void {
    let data: any = this.local.getItem('data');

    if (!_.isEmpty(data)) {
      this.user = JSON.parse(data);
    } else {
      this.nav.push('LoginPage');
    }
  }

  public openOptions(): void {
    let fullName = this.getFullName(this.user);
    let actionSheet = this.actionSheetCtrl.create({
      title: fullName,
      buttons: [
        {
          text: 'Logout',
          handler: () => {
            this.logout();
          }
        },
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  public getFullName(user): string {
    return user.firstName + " " + user.lastName;
  }

  public logout(): void {
    this.local.clear();
    location.reload();
  }

}
