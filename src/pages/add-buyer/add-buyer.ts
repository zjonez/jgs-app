import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddBuyerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-buyer',
  templateUrl: 'add-buyer.html'
})

export class AddBuyerPage {
  constructor(public nav: NavController, public navParams: NavParams) {
  }
}