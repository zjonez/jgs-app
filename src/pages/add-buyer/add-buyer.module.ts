import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBuyerPage } from './add-buyer';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    AddBuyerPage
  ],
  imports: [
    IonicPageModule.forChild(AddBuyerPage),
    ComponentsModule
  ],
})
export class AddBuyerPageModule {}
