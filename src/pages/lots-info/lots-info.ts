import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { PaymentsProvider } from '../../providers/payments/payments';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import * as _ from 'lodash';

/**
 * Generated class for the LotsInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @TODO: this should be renamed to buyer-lot-info page
@IonicPage()
@Component({
  selector: 'page-lots-info',
  templateUrl: 'lots-info.html',
  providers: [
    PaymentsProvider,
    LocalStorageProvider
  ]
})
export class LotsInfoPage {

  public payments;

  public option: string;

  public isApp: boolean;

  public buyerID: String;

  public buyer: Object;
  public lot: Object;
  public agent: Object;
  public data: Object = {};

  constructor(
    public nav: NavController, public navParams: NavParams,
    public paymentService: PaymentsProvider, public platform: Platform,
    public local: LocalStorageProvider
  ) {
    if (!_.isNil(navParams.data) && !_.isEmpty(navParams.data)) {
      this.data = navParams.data;
      this.buyer = navParams.data.buyer;
      this.lot = navParams.data.lot;

      local.setItem('buyer', JSON.stringify(this.buyer));
      local.setItem('lot', JSON.stringify(this.lot));
    } else if (!_.isNil(local.getItem('buyer')) && !_.isEmpty(local.getItem('lot'))) {
      this.buyer = JSON.parse(local.getItem('buyer'));
      this.lot = JSON.parse(local.getItem('lot'));
      this.data = {
        buyer: this.buyer,
        lot: this.lot
      }
    } else {
      this.local.clear();
      window.location.href = '/';
    }

    this.option = 'info';

    this.platform = platform;
    this.isApp = this.platform.is('core') ? false : true;

    this.getPaymentsFromService();
  }

  public getPaymentsFromService() {
    this.paymentService.getPayments().then(
      (payments) => {
        this.payments = payments;
      }
    )
  }

  public openAddPayment() {
    this.nav.push("AddPaymentPage", this.data);
  }
}
