import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotsInfoPage } from './lots-info';

import { LotInfoFormComponent } from '../../components/lot-info-form/lot-info-form';
import { PaymentHistoryFormComponent } from '../../components/payment-history-form/payment-history-form';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';



@NgModule({
  declarations: [
    LotsInfoPage,
    LotInfoFormComponent,
    PaymentHistoryFormComponent
  ],
  imports: [
    IonicPageModule.forChild(LotsInfoPage),
    PipesModule,
    ComponentsModule
  ],
})
export class LotsInfoPageModule {}
