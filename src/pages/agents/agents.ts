import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { AgentsProvider } from '../../providers/agents/agents';
import * as _ from 'lodash';

/**
 * Generated class for the AgentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agents',
  templateUrl: 'agents.html'
})

export class AgentsPage implements OnInit {

  public agents;
  public loading: any = Loading;

  public temporaryPhoto: String = 'http://placehold.it/32x32';

  constructor(
    public nav: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public agentService: AgentsProvider
  ) {
  }

  ngOnInit() {
    this.getAgentsFromService();
  }

  public getAgentsFromService(){
    this.showLoading();

    setTimeout(() => {
      this.agentService
        .getAgents()
        .then((response: any) => {
          this.loading.dismiss();
          if(!response.success) {
            this.showError(response.message);
          } else if (!_.isNil(response.results)) {
            this.agents = response.results;
          }
        })
    }, 2000);
  }

  public openAgentInfo(agent: Object) {
    this.nav.push("AgentInfoPage", agent);
  }

  public openAddAgent(agent: Object) {
    this.nav.push("AddAgentPage", agent);
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public showError(text) {
    text = !(_.isNil(text)) ? text : 'Unknown error occured';

    let alert = this.alertCtrl.create({
      title: 'login failed',
      subTitle: text,
      buttons: ['OK']
    });

    alert.present()
  }
}
