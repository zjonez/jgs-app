import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgentsPage } from './agents';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AgentsPage,
  ],
  imports: [
    IonicPageModule.forChild(AgentsPage),
    PipesModule
  ],
})
export class AgentsPageModule {}
