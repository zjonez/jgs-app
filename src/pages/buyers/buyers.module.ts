import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyersPage } from './buyers';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    BuyersPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyersPage),
    PipesModule,
    ComponentsModule
  ],
})
export class BuyersPageModule {}
