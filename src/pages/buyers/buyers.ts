import { Component, OnInit } from "@angular/core";
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams
} from "ionic-angular";
import * as _ from "lodash";
import { BuyersProvider } from "../../providers/buyers/buyers";

const MIN_CHARS = 2;
@IonicPage()
@Component({
  selector: "page-buyers",
  templateUrl: "buyers.html"
})
export class BuyersPage implements OnInit {
  public buyers;
  public searchString: String;
  public loading: any = Loading;

  public temporaryPhoto: String = "http://placehold.it/32x32";

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public buyerService: BuyersProvider
  ) {}

  ngOnInit() {
    this.getBuyersFromService(null);
  }

  onInput(event) {
    if (!_.isNil(this.searchString) && this.searchString.length >= MIN_CHARS) {
      this.getBuyersFromService(this.searchString);
    } else if(_.isNil(this.searchString) || _.isEmpty(this.searchString)) {
      this.getBuyersFromService(null);
    }
  }

  public getBuyersFromService(searchString) {
    this.showLoading();

    setTimeout(() => {
      this.buyerService.getBuyers(searchString).then((response: any) => {
        if (!response.success) {
          this.loading.dismiss();
          this.showError(response.message);
        } else {
          this.buyers = !_.isNil(response.results) ? response.results : [];
          this.loading.dismiss();
        }
      });
    }, 250);
  }

  public openBuyerInfo(buyer: Object) {
    this.nav.push("BuyersInfoPage", buyer);
  }

  public openAddBuyer() {
    this.nav.push("AddBuyerPage");
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public showError(text) {
    text = !_.isNil(text) ? text : "Unknown error occured";

    let alert = this.alertCtrl.create({
      title: "Error",
      subTitle: text,
      buttons: ["OK"]
    });

    alert.present();
  }
}
