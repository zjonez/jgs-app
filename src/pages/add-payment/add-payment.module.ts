import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPaymentPage } from './add-payment';

import { LotPaymentFormComponent } from '../../components/lot-payment-form-component/lot-payment-form-component';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddPaymentPage,
    LotPaymentFormComponent
  ],
  imports: [
    IonicPageModule.forChild(AddPaymentPage),
    PipesModule,
    ComponentsModule
  ],
})
export class AddPaymentPageModule {}
