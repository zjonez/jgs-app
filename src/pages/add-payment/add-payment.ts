import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import * as _ from 'lodash';

/**
 * Generated class for the AddPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-payment',
  templateUrl: 'add-payment.html',
})
export class AddPaymentPage {

  public buyer: Object;
  public lot: Object;
  public data: Object = {};

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public local: LocalStorageProvider
  ) {
    if (!_.isNil(navParams.data) && !_.isEmpty(navParams.data)) {
      this.buyer = navParams.data.buyer;
      this.lot = navParams.data.lot;

      local.setItem('buyer', JSON.stringify(this.buyer));
      local.setItem('lot', JSON.stringify(this.lot));
    } else if (!_.isNil(local.getItem('buyer') && !_.isNil(local.getItem('lot')))) {
      this.buyer = JSON.parse(local.getItem('buyer'));  
      this.lot = JSON.parse(local.getItem('lot'));  
    } else {
      this.local.clear();
      window.location.href = '/';
    }
  }

}
