import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ConfigServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigServiceProvider {

  constructor(public http: HttpClient) {
  }

  public getConfig(): Promise<Object> {
    return new Promise((resolve, reject) => {
      const url = `assets/jgs-config.json`;

      this.http.get(url)
        .toPromise()
        .then(
          res => {
            resolve(res);
          }
        )
    })
  }

}
