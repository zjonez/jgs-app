import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider {

  constructor(public http: HttpClient) {
  }

  public setItem(key, value):void {
    localStorage.setItem(key, value); 
  }

  public getItem(key): any {
    return localStorage.getItem(key);
  }

  public clear(): void {
    localStorage.clear();
  }

}
