import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Http, Headers, Response, Jsonp, RequestOptions } from "@angular/http";

import { ConfigServiceProvider } from "../config-service/config-service";
import * as _ from "lodash";

/*
  Generated class for the BuyersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BuyersProvider {
  private baseUrl: String = "assets/mock-data";

  private apiUrl: String;

  constructor(public http: HttpClient, public config: ConfigServiceProvider) {
    config.getConfig().then((response: any) => {
      if (!_.isNil(response["api-url"])) {
        let apiUrl = response["api-url"];
          this.apiUrl = `${apiUrl}/api/v1/buyers`;
        }
    });
  }

  public getBuyers(searchString): Promise<Object> {
    const url = `${this.apiUrl}`;
    if (_.isNil(searchString)) {
      searchString = undefined;
    } else if (_.isEmpty(searchString)) {
      searchString = null;
    }

    return this.http
      .get(url, {
        params: {
          searchString
        }
      })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public addBuyer(params): Promise<Object> {
    const url = `${this.apiUrl}`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public addLotBuyer(params, buyerID, lotID): Promise<Object> {
    const url = `${this.apiUrl}/${buyerID}/${lotID}`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error("An error occurred", error);
    return Promise.reject(error.message || error);
  }

}
