import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, Jsonp, RequestOptions } from '@angular/http';

import { ConfigServiceProvider } from '../config-service/config-service';
import * as _ from 'lodash';

/*
  Generated class for the LotsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LotsProvider {

  private baseURL: String = 'assets/mock-data';

  private apiUrl: String;
  private apiUrl2: String;
  private apiUrl3: String;

  constructor(public http: HttpClient, public config: ConfigServiceProvider) {
  
    config
      .getConfig()
      .then((response: any) => {
        if(!_.isNil(response['api-url'])) {
          let apiUrl = response['api-url'];
          this.apiUrl = `${apiUrl}/api/v1/lots`;
          this.apiUrl2 = response['api-url']+"/api/v1/lotbuyers";
          this.apiUrl3 = response['api-url']+"/api/v1/lottypes";
        }
      })
  }

  public getLots(): Promise<Object> {
    const url = `${this.apiUrl}`;

    return this.http
      .get(url)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public getLotDetails(params): Promise<Object> {
    const url = `${this.apiUrl}`

    return this.http
      .get(url, {
        params: params
      })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public getLotTypes(): Promise<Object> {
    return new Promise((resolve, reject) => {
      const apiURL = `${this.baseURL}/lot-types.json`;

      this.http.get(apiURL)
        .toPromise()
        .then(
          res => {
            resolve (res);
          }
        )
    })
  }
  /*public getLotTypes(): Promise<Object> {
    const url = `${this.apiUrl3}`;

        return this.http.get(url)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
  }*/

  public addLotInventory(params): Promise<Object> {
    const url = `${this.apiUrl}`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public addLotBuyer(params): Promise<Object> {
    const url = `${this.apiUrl2}`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

}
