import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';

/*
  Generated class for the CommissionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommissionsProvider {

  private baseURL: String = 'assets/mock-data';

  constructor(public http: HttpClient) {
  }

  public getCommissions(): Promise<Object> {
    return new Promise((resolve, reject) => {
      const apiURL = `${this.baseURL}/commissions.json`;

      this.http.get(apiURL)
      .toPromise()
      .then(
        res => {
          resolve(res);
        }
      )
    })
  }
}
