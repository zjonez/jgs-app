import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { ConfigServiceProvider } from '../config-service/config-service';

import * as _ from 'lodash';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// @TODO: this can be moved to employee.ts
export class User {
  name: string;
  email: string;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}

@Injectable()
export class AuthServiceProvider {
  currentUser: any = User;
  public baseUrl: String;

  constructor(public http: HttpClient, public config: ConfigServiceProvider) {
    
    config
      .getConfig()
      .then((response: any) => {
        if(!_.isNil(response['api-url'])) {
          this.setBaseUrl(response['api-url']);
        }
      })
  }

  public setBaseUrl(apiUrl) {
    this.baseUrl = `${apiUrl}/api/v1/users`;
  }

  public login(params): Promise<Object> {
    const url = `${this.baseUrl}/login`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  
  public getUserInfo() : User {
    return this.currentUser;
  }

  public logout(){
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
