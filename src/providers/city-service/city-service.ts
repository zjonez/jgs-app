import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as _ from 'lodash';

/*
  Generated class for the CityServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CityServiceProvider {

  private baseUrl: String = 'assets/data';

  constructor(public http: HttpClient) {
  }

  public getCities(province: Object): Promise<Object> {
    return new Promise((resolve, reject) => {
      const apiURL = `${this.baseUrl}/cities.json`;

      this.http.get(apiURL)
        .toPromise()
        .then(
          res => {
            if (!_.isNil(province)) {
              res = this.filterCities(res, province);
            }
            resolve(res);
          }
        )
    })
  }

  public filterCities(res, province) {
    return _.filter(res, (o) => { return o.province === province.key; });
  }

}
