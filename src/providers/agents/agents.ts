import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, Jsonp, RequestOptions } from '@angular/http';

import { ConfigServiceProvider } from '../config-service/config-service';
import * as _ from 'lodash';

/*
  Generated class for the AgentsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AgentsProvider {

  private baseUrl: String = 'assets/mock-data';

  private apiUrl: String;

  constructor(public http: HttpClient, public config: ConfigServiceProvider) {
    
    config
      .getConfig()
      .then((response: any) => {
        if(!_.isNil(response['api-url'])) {
          let apiUrl = response['api-url'];
          this.apiUrl = `${apiUrl}/api/v1/agents`;
        }
      })
  }

  public getAgents(): Promise<Object> {
    const url = `${this.apiUrl}`;

    return this.http
      .get(url)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  public addAgent(params): Promise<Object> {
    const url = `${this.apiUrl}`;

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

}
