import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response, Jsonp, RequestOptions } from '@angular/http';

import { ConfigServiceProvider } from '../config-service/config-service';

import * as _ from 'lodash';

/*
  Generated class for the PaymentsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaymentsProvider {

  private baseURL: String = 'assets/mock-data';

  private apiUrl: String;

  constructor(public http: HttpClient, public config: ConfigServiceProvider) {
    
    config
      .getConfig()
      .then((response: any) => {
        if(!_.isNil(response['api-url'])) {
          this.apiUrl = response['api-url']+"/api/v1/payments";
        }
      }) 
  }

  public getPayments(): Promise<Object> {
    return new Promise((resolve, reject) => {
      const apiURL = `${this.baseURL}/payments.json`;

      this.http.get(apiURL)
      .toPromise()
      .then(
        res => {
          resolve(res);
        }
      )
    })
  }

  public getTransactionTypes(): Promise<Object> {
    return new Promise((resolve, reject) =>{
      const apiURL = `${this.baseURL}/transaction-types.json`;

      this.http.get(apiURL)
      .toPromise()
      .then(
        res => {
          resolve(res);
        }
      )
    })
  }

  public addPayment(params): Promise<Object> {
    const url = `${this.apiUrl}`;
    console.log('url');
    console.log(url);

    return this.http
      .post(url, params)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }

}
