import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProvinceServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvinceServiceProvider {

  private baseUrl:String = 'assets/data';

  constructor(public http: HttpClient) {
  }

  public getProvinces(): Promise<Object> {
    return new Promise((resolve, reject) => {
      const apiURL = `${this.baseUrl}/provinces.json`;

      this.http.get(apiURL)
      .toPromise()
      .then(
        res => {
          resolve(res);
        }
      )
    })
  }

}
